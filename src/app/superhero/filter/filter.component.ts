import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-superhero-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class SuperHeroFilterComponent {
  filterForm: FormGroup = new FormGroup({
    filter: new FormControl('')
  })
  constructor(
  ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    //filtro
    console.log("Acá se implementara el filtro respectivo")
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-superhero-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class SuperHeroListComponent implements OnInit {

  superHeroes: any[] = [{
    id: '1',
    name: 'Hero Name',
    intellgence: '38',
    speed: '17',
    power: '24',
    gender: 'Male',
    race: 'Human',
    images: '',
  }, {
    id: '2',
    name: 'Hero Name',
    intellgence: '38',
    speed: '17',
    power: '24',
    gender: 'Male',
    race: 'Human',
    images: '',
  }, {
    id: '3',
    name: 'Hero Name',
    intellgence: '38',
    speed: '17',
    power: '24',
    gender: 'Male',
    race: 'Human',
    images: '',
  },
  {
    id: '4',
    name: 'Hero Name',
    intellgence: '38',
    speed: '17',
    power: '24',
    gender: 'Male',
    race: 'Human',
    images: '',
  },
  {
    id: '5',
    name: 'Hero Name',
    intellgence: '38',
    speed: '17',
    power: '24',
    gender: 'Male',
    race: 'Human',
    images: '',
  },
  {
    id: '6',
    name: 'Hero Name',
    intellgence: '38',
    speed: '17',
    power: '24',
    gender: 'Male',
    race: 'Human',
    images: '',
  }]

  constructor(
  ) { }

  ngOnInit(): void {
    
  }

}
